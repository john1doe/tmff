GitLab CI/CD example
====================

This is a dummy repo for the demonstration of GitLab's CI/CD capabilities with `pandoc`.

The following resources may be of some interest:

  - [DevOps for Presentations: Reveal.js, Markdown, Pandoc, GitLab CI](https://medium.com/isovera/devops-for-presentations-reveal-js-markdown-pandoc-gitlab-ci-34d07d2c1011)
  - [Document generation using Markdown and Pandoc](http://gbraad.nl/blog/document-generation-using-markdown-and-pandoc.html)
  - [ntwrkguru/pandoc-gitlab-ci](https://hub.docker.com/r/ntwrkguru/pandoc-gitlab-ci/)
